console.log("Hello");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime",
    "Jap",
    "Japs",
    "Jap1",
    "Jap2",
    "Jap3",
    "Jap4",
    "Jap5"
];

let friendsList = [
	"Jap1",
    "Jap2",
    "Jap3",
    "Jap4",
    "Jap5"
];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(registerUser){
	let confirming = registeredUsers.includes(registerUser);
	// console.log(confirming);

		if (confirming === true){

			alert("Registration failed. Username already exists!")
			console.warn("Registration failed");

		} else {
			let addingUser = registeredUsers.push(registerUser);

			console.log("User " + registerUser + " registered" );
			//console.log(registeredUsers);
			alert("Thank you for registering!");
			
		}

}

//console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(friend){
	let confirming = registeredUsers.includes(friend);
	// console.log(confirming);

		if (confirming === true){
			let confirmingFriend = friendsList.includes(friend);

				if (confirmingFriend === true){

					alert("You have already added " + friend + " as a friend!");
					console.warn(friend + " is already added to friends list");
					
				} else {
					let addingFriend = friendsList.push(friend)

					alert("You have added " + friend + " as a friend!")
					console.log(friend + " added to friends list");;
				}

		} else {

			console.warn("User not found." );
			alert("User not found.");
			
		}

}
	
//console.log(friendsList)

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    function displayFriends(friends){
    	
    	if (friendsList.length === 0){

    		alert("You currently have 0 friends. Add one first.")
    		console.warn("You currently have 0 friends");

    	} else {

    		console.log("Friends List:")
    		friendsList.forEach(function(friends){
    		console.log(friends);
    		});
    	}
    }

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends(numfriends){
    	
    	if (friendsList.length === 0){

    		alert("You currently have 0 friends. Add one first.");
    		console.warn("You currently have 0 friends");

    	} else {

    		alert("You currently have " + friendsList.length + " friends.");
    		console.log("You have " + friendsList.length + " friends");
    	}
    }


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteFriend(friendToDel){
    	
    	if (friendsList.length === 0){

    		alert("You currently have 0 friends. Add one first.");
    		console.warn("You currently have 0 friends");

    	} else {
    		let lastFriend = (friendsList[friendsList.length-1]);
    		let removeLastFriend = friendsList.pop();

    		console.warn("Removed " + lastFriend + " from friends list");
    	}
    }



/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

	function deleteAFriend(aFriend){
    	
    	if (friendsList.length === 0){

    		alert("You currently have 0 friends. Add one first.");
    		console.warn("You currently have 0 friends");

    	} else {
    		let scanFriend = friendsList.indexOf(aFriend);

    		//console.log(scanFriend);

	    		if (scanFriend === 0){
	    			friendsList.splice(0,1);

	    			console.warn("You deleted " + aFriend + " from friends list");
	    			//console.log(scanFriend);

	    		} else if (scanFriend > 0){
	    			friendsList.splice(scanFriend,1);

	    			console.warn("You deleted " + aFriend + " from friends list");
	    			//console.log(scanFriend);

	    		} else {

	    			console.log("User not in friends friends list");
	    		}
    	}
    }





