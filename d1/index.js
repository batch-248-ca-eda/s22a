console.log("JavaScript - Array Manipulation");

// Array Methods
// JS has built-in functions and methods for arrays
// This allows us to manipulate and access array items

// Mutator methods
/*

	Mutator Methods are functions that "mutate" or change an array after they are created 
	These methods manipulate the original array performing various tasks such as adding and removing elements

*/

let fruits = ["Apple","Orange","Kiwi","Dragon Fruit"];

// push
	/*
		adds an element in the end of an array and returns the array length

		Syntax:

		arrayName.push();

	*/

	console.log("Curren Array: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);// Mango is added at the end of the array

	fruits.push("Avocado","Guava");
	console.log("Mutated array from push method: ");
	console.log(fruits);// "Avocado" and "Guava" is added at the end of the array

// Pop

/*
	Removes the last element in an array and returns the removed element
	Syntax:

	arrayName.push();

*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);// Guava
	console.log("Mutated array from pop method: ");
	console.log(fruits);// Guava is removed
	// ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']




	let ghostFighters = ["Eugene","Dennis","Alfred","Taguro"];

	/*
		Mini Activity 1
		Create a function which will remove the last person in the array
		log the ghostFighters array in the console
		Send an ss of your output

	*/


	console.log("Current array");
	console.log(ghostFighters);
	let removedLastPer = ghostFighters.pop();
	console.log("Removed last person in the array");
	console.log(ghostFighters)

/*
// Solution mini act 1

	function removedPerson(){
		ghostFighters.pop();
	}

	removedPerson();
	console.log(ghostFighters);


// unshift()
*/


/*

	adds one ore more elements at the beginning of an array

	syntax
		arrayName.unshift("elementA");
		arrayName.unshift("elementA, elementB");

*/
	
	console.log("Current array");
	console.log(fruits);
	fruits.unshift("Lime","Banana");
	console.log("Mutated array from unshift method: ");
	console.log(fruits);// 




// shift()

/*

	removes an element at the beginning of an array AND returns the removed element

	syntax
		arrayName.shift();

*/
	console.log("Current array");
	console.log(fruits);
	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits);// removed "Lime" from the array //['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


// Splice

/*
	simultaneously removes elements from a specified index number and adds the elements

	Syntax
	arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

*/
	
	console.log("Current array");
	console.log(fruits);
	fruits.splice(1,2,"Lime","Cherry");
	console.log("Mutated array from splice method: ");
	console.log(fruits);// Removed "Apple" and "Orange" fruits.splice(1,2) and added "Lime" and "Cherry"


// Sort


	console.log("Current array");
	console.log(fruits);
	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);


// Reverse

	console.log("Current array");
	console.log(fruits);
	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


// Non-Mutator Methods

/*
	Non-Mutator methods are functions that do not modify or change an array after they are created
	-these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output

*/

	let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];

	// indexOf()

	/*
		returns the index number of the first matching element found in an array
		--if no match is found, the result is -1
		-the search process will be done from the first element processing to the last element


		syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue,fromIndex);


	*/

		let firstIndex = countries.indexOf("CAN");
		console.log("Result of indexOf method: " + firstIndex);//1
		let firstExample = countries.indexOf("ph");
		console.log("Result of indexOf method: " + firstExample);//-1
		let invalidCountry = countries.indexOf("PH");
		console.log("Result of indexOf method: " + firstIndex);//1


	// lastIndexOf()

		/*
			reutrns the index of the last matching element found in an array
			-the search process will be dine form last element processing to the first element

			synax: 
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue,fromIndex);

		*/

		//["US","PH","CAN","SG","TH","PH","FR","DE"]

		let lastIndex = countries.lastIndexOf("PH",3);
		console.log("Result of lastIndexOf method: " + lastIndex);//1


	// slice()

		/*
			-portions/slices elements from an array AND returns a new array
			-syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex,endingIndex);

		*/

		console.log(countries);//['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

		let slicedArrayA = countries.slice(2);
		console.log("Result from slice method: ");
		console.log(slicedArrayA);//['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

		console.log(countries);//Nothings changes, no element is removed or deleted

		let slicedArrayB = countries.slice(2,4);
		console.log("Result from slice method: ");
		console.log(slicedArrayB);// "CAN","SG" //[02,03]

		let slicedArrayc = countries.slice(-3);
		console.log("Result from slice method: ");
		console.log(slicedArrayc);// ['PH', 'FR', 'DE']


	// toString()

		/*
			returns an array as a string separated by commas
			Synax: 
			arrayName.toString();

		*/

		let stringArray = ghostFighters.toString();
		console.log("Result from toString method: ");
		console.log(stringArray);//Eugene,Dennis
		console.log(typeof stringArray);//string


	// concat()

		/*
			combine two arrays and returns the combined result
			-Syntax
				arrayA.concat(arrayB);
				arrayA.concat(elementA);

		*/

		let taskArrayA = ['drink html','eat JavaScript'];
		let taskArrayB = ['inhale css','breathe react'];
		let taskArrayC = ['get git','be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log('Result from concat method: ');
		console.log(tasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react']

		// combine multiple arrays

		console.log("Result from concat method: ");
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log(allTasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react', 'get git', 'be node']

		// combine arrays with elements
		let combinedTasks = taskArrayA.concat('smell express','throw mongodb');
		console.log("Result from concat method: ");
		console.log(combinedTasks);// ['drink html', 'eat JavaScript', 'smell express', 'throw mongodb']


	// join

		/*
			Returns an array as a string separated by specified separator
			-syntax


			arrayName.join('separatorString');

		*/

		let users = ['John','Jane','Joe','Robert','Neil'];
		console.log(users.join());//John,Jane,Joe,Robert,Neil
		console.log(users.join(' '));//John Jane Joe Robert Neil
		console.log(users.join(' - '));//John - Jane - Joe - Robert - Neil	
		console.log(users.join(' * '));//John * Jane * Joe * Robert * Neil


	// Iteration Method

	/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks

	*/

// forEach
	
	/*
		similar to for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-anonymoys function is able to receive the current item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural form of the data stored in an array 
		-it's common practice to use the singular form of the array content for parameter names used in array loops
		-forEach() does NOT return anything

		Synrax
		arrayName.forEach(function(indivElement){
			statement
		})

	*/

		// ['drink html', 'eat JavaScript', 'inhale css', 'breathe react', 'get git', 'be node']

		allTasks.forEach(function(task){
			console.log(task);

		});



		// MINI ACT 2
		function displayGhostFighters(){
			ghostFighters.forEach(function(fighter){
			console.log(fighter);
			});
		}
		displayGhostFighters();


	// use forEach with conditional statements

	let filteredTasks = [];

	allTasks.forEach(function(task){
		console.log(task);

		// we "filtered out" tasks with greater than 10 characters
		if(task.length>10){
			console.log(task + " my length is greater than 10");
			// we added/pushed the "tasks" filtered into the filteredTasks array
			// it is no longer an empy array
			filteredTasks.push(task);
		}

	});

	console.log("Result of filtered Tasks: ");
	console.log(filteredTasks);//before it was an empy array [] // after ['eat JavaScript', 'breathe react']


// map()

	/*
		iterates on each element and returns NEW ARRAY with different VALUES depending on the RESULT of the function's operation

	*/

	
	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(number){
		return number * number;
	});

	console.log("Original Array: ");
	console.log(numbers);//[1,2,3,4,5];
	console.log("Result of map method: ");
	console.log(numberMap);//[1,4,9,16,25];

	// map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;

	});

	console.log(numberForEach);//undefined because forEach doesnt return anything 


// every()

/*

	check if all elements in an array meet the given condition

*/

	//[1,2,3,4,5]

	let allValid = numbers.every(function(number){
		return (number < 3);
	});

	console.log(allValid);//false

	// some()
		
		/*

			check if at least onf element meet the given condition

		*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	});
	console.log(someValid);//true

	// filter()

	let filterValid = numbers.filter(function(number){
		return (number < 3);
	});

	console.log(filterValid);//[1,2]


	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	});
	console.log(nothingFound);//[]

	// filtering useing forEach

	let filteredNumbers = [];

	numbers.forEach(function(number){

		if (number < 3){
			filteredNumbers.push(number);
		}
	});

	console.log(filteredNumbers);//[1,2]


// includes()

	/*



	*/
	let products = ['Mouse','Keyboard','Laptop','Monitor'];

	let productFound = products.includes('Mouse');
	console.log(productFound);//case sensitive //true

	let productNotFound = products.includes("Headset");
	console.log(productNotFound);//false


// Method chaining 
	
	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});

	console.log(filteredProducts);//['Keyboard', 'Laptop']


// reduce

	/*

		
		syntax:
		let/const resultArray - arrayName.reduce(function(accumulator, currentValue){
			
		})

	*/



	console.log(numbers);

	let iteration = 0;

	let reducedArray = numbers.reduce(function(x,y){

		console.warn("Current iteration: " + ++iteration);
		console.log('accumulator: ' + x);
		console.log('current value: ' + y);

		return x + y;

	});

	console.log("Result of reduce method: " + reducedArray);

	let iterationStr = 0;

	let list = ["Hello",'Again','World'];

	let reducedJoin = list.reduce(function(x,y){

		console.warn("Current iteration: " + ++iterationStr);
		console.log('accumulator: ' + x);
		console.log('current value: ' + y);

		return x + ' ' + y;

	});

	console.log("Result of reduce method: " + reducedJoin);